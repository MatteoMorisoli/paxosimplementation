#/bin/bash

find ./src/ -name *.java > source.txt &
javac  -d ./build @source.txt &
java -Djava.net.preferIPv4Stack=true -cp ./build/ inf.usi.Proposer $1 $2
