package inf.usi;

public class ValueRecord {
    private final int decidedValue;
    private final int firstRound;
    private final int clientId;

    public ValueRecord(int decidedValue, int firstRound, int clientId) {
        this.decidedValue = decidedValue;
        this.firstRound = firstRound;
        this.clientId = clientId;
    }

    public int getDecidedValue() {
        return decidedValue;
    }

    public int getFirstRound() {
        return firstRound;
    }

    public int getClientId() {
        return clientId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ValueRecord that = (ValueRecord) o;

        if (decidedValue != that.decidedValue) return false;
        if (firstRound != that.firstRound) return false;
        return clientId == that.clientId;
    }

    @Override
    public int hashCode() {
        int result = decidedValue;
        result = 31 * result + firstRound;
        result = 31 * result + clientId;
        return result;
    }

    @Override
    public String toString(){
        return "" + clientId + " " + firstRound + " " + decidedValue;
    }
}
