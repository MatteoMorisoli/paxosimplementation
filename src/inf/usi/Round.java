package inf.usi;


import java.io.Serializable;

public class Round implements Serializable{

    private int round;
    private final int firstProposedRound;
    private final int proposerId;
    private final int clientId;

    public Round(int round, int proposerId, int clientId) {
        this.round = round;
        this.proposerId = proposerId;
        this.clientId = clientId;
        this.firstProposedRound = round;
    }


    public int getFirstProposedRound() {
        return firstProposedRound;
    }

    public int getRound() {
        return round;
    }

    public void incrementRound() {
        this.round = round++;
    }

    public int getProposerId() {
        return proposerId;
    }

    public int getClientId() {
        return clientId;
    }

    public boolean greaterOrEqual(Round round){
        if(round == null){
            return true;
        }
        if(this.round > round.getRound()){
            return true;
        }else if(this.round == round.getRound()){
            if(this.proposerId >= round.getProposerId()){
                return true;
            }
        }
        return false;
    }

    public boolean greater(Round round){
        if(round == null){
            return true;
        }
        if(this.round > round.getRound()){
            return true;
        }else if(this.round == round.getRound()){
            if(this.proposerId > round.getProposerId()){
                return true;
            }
        }
        return false;
    }

    public boolean equal(Round round){
        if(round == null){
            return false;
        }
        if(this.round == round.getRound() && this.proposerId == round.getProposerId()){
            return true;
        }else{
            return false;
        }
    }
}
