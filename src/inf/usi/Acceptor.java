package inf.usi;

import inf.usi.messages.*;

import java.io.IOException;
import java.io.Serializable;
import java.net.SocketTimeoutException;
import java.util.Map;

public class Acceptor {
    private final int id;
    private Round round;
    private Round votedRound;
    private int votedValue;
    private final MultiCaster acceptorsChannel;
    private final MultiCaster proposersChannel;

    public Acceptor(int id, NetworkInfo acceptorsInfo, NetworkInfo proposersInfo) {
        this.id = id;
        this.acceptorsChannel = new MultiCaster(acceptorsInfo, true, 15000);
        this.proposersChannel = new MultiCaster(proposersInfo, false);
        this.round = null;
        this.votedRound = null;
        this.votedValue = -1;
    }

    public static void main(String[] args) {
        int id = Integer.parseInt(args[0]);
        ConfigReader cr = new ConfigReader(args[1]);
        Map<String, NetworkInfo> infos = cr.readConfig();
        Acceptor acceptor = new Acceptor(id, infos.get("acceptors"), infos.get("proposers"));
        acceptor.loop();
    }

    private void loop() {
        Serializable serializedMessage;
        while(true){
            try {
                serializedMessage = acceptorsChannel.receive();
                if (serializedMessage instanceof ProposerAcceptor1AMessage) {
                    ProposerAcceptor1AMessage acceptor1AMessage = (ProposerAcceptor1AMessage) serializedMessage;
                    if (acceptor1AMessage.getCurrentRound().greater(round)) {
                        round = acceptor1AMessage.getCurrentRound();
                        AcceptorProposer1BMessage proposer1BMessage = new AcceptorProposer1BMessage(id, round, votedRound, votedValue);
                        proposersChannel.send(proposer1BMessage);
                    }
                } else if (serializedMessage instanceof ProposerAcceptor2AMessage) {
                    ProposerAcceptor2AMessage acceptor2AMessage = (ProposerAcceptor2AMessage) serializedMessage;
                    if (acceptor2AMessage.getCurrentRound().greaterOrEqual(round)) {

                        votedRound = acceptor2AMessage.getCurrentRound();
                        votedValue = acceptor2AMessage.getCurrentValue();
                        AcceptorProposer2BMessage proposer2BMessage = new AcceptorProposer2BMessage(id, votedRound, votedValue);
                        proposersChannel.send(proposer2BMessage);
                    }
                }
                serializedMessage = null;
            }catch(SocketTimeoutException e){
                LeaderOracle.changeLeader();
            }catch (IOException e){

            }
        }
    }
}
