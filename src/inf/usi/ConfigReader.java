package inf.usi;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ConfigReader {

    private String fileName;

    public ConfigReader(String filename){
        this.fileName = filename;
    }

    public Map<String, NetworkInfo> readConfig(){
        Map<String, NetworkInfo> configInfo = new HashMap<String, NetworkInfo>();
        try (BufferedReader buffReader = new BufferedReader(new FileReader(fileName))) {
            String line;
            while ((line = buffReader.readLine()) != null) {
                String[] infos = line.split(" +");
                try{
                    configInfo.put(infos[0], new NetworkInfo(infos[1], infos[2]));
                }catch (IndexOutOfBoundsException e){
                    System.out.println("malformed output");
                }
            }
        } catch (IOException e) {
            System.out.println("file not found");
            e.printStackTrace();
        }
        return configInfo;
    }
}
