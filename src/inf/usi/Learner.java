package inf.usi;

import inf.usi.messages.CatchUpMessage;
import inf.usi.messages.LearnerClientMessage;
import inf.usi.messages.LearnerProposerMessage;
import inf.usi.messages.ProposerLearnerMessage;

import java.io.IOException;
import java.io.Serializable;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Learner {
    private final int id;
    private final MultiCaster learnersChannel;
    private final MultiCaster clientsChannel;
    private final MultiCaster proposersChannel;
    private  List<Integer> printedValues;
    private final int CHUNK_SIZE = 50;

    public Learner(int id, NetworkInfo learnersInfo, NetworkInfo clientsInfo, NetworkInfo proposersInfo) {
        this.id = id;
        this.learnersChannel = new MultiCaster(learnersInfo, true, 5000);
        this.clientsChannel = new MultiCaster(clientsInfo, false);
        this.proposersChannel = new MultiCaster(proposersInfo, false);
        this.printedValues = new ArrayList<>();
    }

    public static void main(String[] args) {
        int id = Integer.parseInt(args[0]);
        ConfigReader configReader = new ConfigReader(args[1]);
        Map<String, NetworkInfo> infos = configReader.readConfig();
        Learner learner = new Learner(id, infos.get("learners"), infos.get("clients"), infos.get("proposers"));
        learner.loop();
    }

    private void loop() {
        Serializable serializableMessage = null;
        boolean print = true;
        LearnerProposerMessage learnerMessage = new LearnerProposerMessage(id);
        proposersChannel.send(learnerMessage);
        boolean catching = true;
        List<Integer> catchUpList = new ArrayList<>();
        int messageNum = 0;
        while(true) {
            print = true;
            try {
                serializableMessage = learnersChannel.receive();
            } catch (SocketTimeoutException e) {
                print = false;
                catching = false;
            } catch (IOException e) {

            }
            if(serializableMessage instanceof CatchUpMessage && catching){
                CatchUpMessage catchUp = (CatchUpMessage) serializableMessage;
                if(catchUp.getLearnerId() == id) {
                    catchUpList.addAll(catchUp.getOrderedValues());
                    int messagesToPrint = catchUp.getOrderedValues().size();
                    for (int i = 0; i < messagesToPrint; i++) {
                        System.out.println(catchUpList.get(CHUNK_SIZE* messageNum + i));
                    }
                    messageNum++;
                    if(!catchUp.isMoreMessages()) {
                        catching = false;
                        printedValues = catchUpList;
                    }

                }
            } else if((serializableMessage instanceof ProposerLearnerMessage) && print){
                ProposerLearnerMessage proposerMessage = (ProposerLearnerMessage) serializableMessage;
                printedValues.add(proposerMessage.getDecidedValue());
                LearnerClientMessage clientMessage = new LearnerClientMessage(proposerMessage.getClientId(),proposerMessage.getDecidedValue());
                clientsChannel.send(clientMessage);
                if(!catching) {
                    System.out.println(proposerMessage.getDecidedValue());
                }
            }
        }
    }
}
