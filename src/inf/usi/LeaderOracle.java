package inf.usi;

public class LeaderOracle {
    public static int proposerNumber;
    public static int leaderId = 0;



    public static int getLeaderId(){
        return leaderId + 1;
    }

    public static void seekTheOracle(){
        proposerNumber++;
    }

    public static void changeLeader(){
        leaderId = proposerNumber - ((leaderId + 1) % proposerNumber);
    }
}
