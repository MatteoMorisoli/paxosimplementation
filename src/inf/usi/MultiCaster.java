package inf.usi;

import java.io.*;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

public class MultiCaster {
    private int port;
    private InetAddress ip;
    private MulticastSocket socket;
    private final boolean receiving;

    public MultiCaster(NetworkInfo info, boolean receiving, int timeout) {
        this.port = info.getPort();
        this.ip = info.getIp();
        this.receiving = receiving;
        try {
            this.socket = new MulticastSocket(port);
//            this.socket.setSendBufferSize(socket.getSendBufferSize() * 4);
//            this.socket.setReceiveBufferSize(socket.getReceiveBufferSize() * 4);
//            this.socket.setTimeToLive(2);
            if(this.receiving) {
                this.socket.joinGroup(this.ip);
                this.socket.setSoTimeout(timeout);
            }
        } catch (IOException e) {
            System.out.println("Error during socket creation");
            e.printStackTrace();
        }
    }

    public MultiCaster(NetworkInfo info, boolean receiving) {
        this.port = info.getPort();
        this.ip = info.getIp();
        this.receiving = receiving;
        try {
            this.socket = new MulticastSocket(port);
//            this.socket.setSendBufferSize(socket.getSendBufferSize() * 4);
//            this.socket.setReceiveBufferSize(socket.getReceiveBufferSize() * 4);
//            this.socket.setTimeToLive(2);
            if(this.receiving) {
                this.socket.joinGroup(this.ip);
            }
        } catch (IOException e) {
            System.out.println("Error during socket creation");
            e.printStackTrace();
        }
    }

    public void send(Serializable message){
        ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream();
        try {
            ObjectOutputStream os = new ObjectOutputStream(byteOutputStream);
            os.writeObject(message);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Error with byteStreams in multicaster");
        }
        byte[] messageByte = byteOutputStream.toByteArray();
        DatagramPacket dataMessage =  new DatagramPacket(messageByte, messageByte.length,
                ip, port);
        try {
            socket.send(dataMessage);
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            System.out.println("Error during send");
            e.printStackTrace();
        }
    }

    public Serializable receive() throws IOException {
        Serializable serializableObject = null;
        if(!receiving){
            System.out.println("socket is in send-only mode");
            return null;
        }else{
            byte[] buffer = new byte[16000];
            DatagramPacket message = new DatagramPacket(buffer, buffer.length);
            try {
                socket.receive(message);
                buffer = message.getData();
                ByteArrayInputStream byteInputStream = new ByteArrayInputStream(buffer);
                ObjectInputStream objectInputStream = new ObjectInputStream(byteInputStream);
                serializableObject = (Serializable) objectInputStream.readObject();
            } catch (ClassNotFoundException e) {
                System.out.println("class not found in multicaster");
            }
            return serializableObject;
        }
    }
}
