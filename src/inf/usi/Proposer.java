package inf.usi;

import inf.usi.messages.*;

import java.io.IOException;
import java.io.Serializable;
import java.net.SocketTimeoutException;
import java.util.*;

public class Proposer {

    private final int id;
    private final MultiCaster clientsChannel;
    private final MultiCaster proposersChannel;
    private final MultiCaster acceptorsChannel;
    private final MultiCaster learnersChannel;
    private boolean active = false;
    private final List<ValueRecord> recordedValue;
    private final List<Integer> decidedValues;
    private final int ACCEPTOR_NUMBER = 3;
    private final int CHUNK_SIZE = 50;
    private int roundCounter;
    private List<Integer> lastValue;

    public Proposer(int id, NetworkInfo proposersInfo, NetworkInfo clientsInfo, NetworkInfo acceptorsInfo, NetworkInfo learnersInfo){
        this.id = id;
        this.proposersChannel = new MultiCaster(proposersInfo, true, 3500);
        this.clientsChannel = new MultiCaster(clientsInfo, false);
        this.learnersChannel = new MultiCaster(learnersInfo, false);
        this.acceptorsChannel = new MultiCaster(acceptorsInfo, false);
        this.decidedValues = new ArrayList<>();
        this.roundCounter = 1;
        this.recordedValue = new ArrayList<>();
        if(id == 1){
            active = true;
        }
    }

    public static void main(String[] args) {
        int id = Integer.parseInt(args[0]);
        ConfigReader cr = new ConfigReader(args[1]);
        Map<String, NetworkInfo> infos = cr.readConfig();
        Proposer proposer = new Proposer(id, infos.get("proposers"), infos.get("clients"), infos.get("acceptors"), infos.get("learners"));
        LeaderOracle.seekTheOracle();
        proposer.loop();
    }

    private void loop(){
        while(true){
            if(LeaderOracle.getLeaderId() == id){
                try {
                    Serializable serializedMessage = proposersChannel.receive();
                    if(serializedMessage instanceof LearnerProposerMessage){
                        LearnerProposerMessage catchupRequest = (LearnerProposerMessage) serializedMessage;
                        for(int chunk = 0; chunk < decidedValues.size(); chunk += CHUNK_SIZE){
                            if(chunk + CHUNK_SIZE > decidedValues.size() -1){
                                ArrayList<Integer> subList1 = new ArrayList<>(decidedValues.subList(chunk, decidedValues.size()));
                                CatchUpMessage catchupAnswer = new CatchUpMessage(subList1, catchupRequest.getLearnerId(), false);
                                learnersChannel.send(catchupAnswer);
                            }else{
                                ArrayList<Integer> subList2 = new ArrayList<>(decidedValues.subList(chunk, chunk+CHUNK_SIZE));
                                CatchUpMessage catchupAnswer = new CatchUpMessage(subList2, catchupRequest.getLearnerId(), true);
                                learnersChannel.send(catchupAnswer);
                            }
                        }
                    }else if(serializedMessage instanceof ClientProposerMessage){
                        this.paxos((ClientProposerMessage)serializedMessage);
                    }
                } catch (SocketTimeoutException e) {

                }catch(IOException e){

                }
            }
        }
    }

    private void paxos(ClientProposerMessage message){
        int phase = 1;
        int kValue = -1;
        Set<Integer> acceptorSet1 = new HashSet<>();
        Set<Integer> acceptorSet2 = new HashSet<>();
        Round round = new Round(roundCounter, id, message.getClientId());
        boolean paxos = true;
        Serializable serializedMessage;
        while(paxos) {
            try {
                switch (phase) {
                    case 1:
                        kValue = message.getValueToSend();
                        if(!decidedValues.isEmpty() && kValue == decidedValues.get(decidedValues.size() -1)){
                            paxos = false;
                            break;
                        }
                        ProposerAcceptor1AMessage proposer1AMessage = new ProposerAcceptor1AMessage(id, message.getClientId(), round);
                        acceptorsChannel.send(proposer1AMessage);
                        phase++;
                        break;
                    case 2:
                        serializedMessage = proposersChannel.receive();
                        if (serializedMessage instanceof AcceptorProposer1BMessage) {
                            AcceptorProposer1BMessage acceptor1BAnswer = (AcceptorProposer1BMessage) serializedMessage;
                            if (round.equal(acceptor1BAnswer.getRound())) {
                                acceptorSet1.add(acceptor1BAnswer.getAcceptorId());
//                                if (k < acceptor1BAnswer.getVotedRound()) {
//                                    k = acceptor1BAnswer.getVotedRound();
//                                    //kValue = acceptor1BAnswer.getVotedValue();
//                                }
                            }
                            if (acceptorSet1.size() >= ACCEPTOR_NUMBER - 1) {
                                ProposerAcceptor2AMessage acceptor2AMessage = new ProposerAcceptor2AMessage(round, kValue, id);
                                acceptorsChannel.send(acceptor2AMessage);
                                phase++;
                                kValue = -1;
                            }
                        }
                        serializedMessage = null;
                        break;
                    case 3:
                        //System.out.println("phase 3");
                        serializedMessage = proposersChannel.receive();
                        if (serializedMessage instanceof AcceptorProposer2BMessage) {
                            //System.out.println("Received second message from acceptors");
                            AcceptorProposer2BMessage acceptor2BAnswer = (AcceptorProposer2BMessage) serializedMessage;
                            if (acceptor2BAnswer.getVotedRound().equal(round)) {
                                acceptorSet2.add(acceptor2BAnswer.getAcceptorId());
                            }
                            if (acceptorSet2.size() >= ACCEPTOR_NUMBER - 1) {
                                ProposerLearnerMessage learnerMessage = new ProposerLearnerMessage(acceptor2BAnswer.getVotedRound().getClientId(), acceptor2BAnswer.getVotedValue());
                                //ValueRecord record = new ValueRecord(acceptor2BAnswer.getVotedValue(), acceptor2BAnswer.getVotedRound().getFirstProposedRound(), acceptor2BAnswer.getVotedRound().getClientId());
                                //if (!recordedValue.contains(record)){
                                    //recordedValue.add(record);
                                learnersChannel.send(learnerMessage);
                                decidedValues.add(acceptor2BAnswer.getVotedValue());
                                paxos = false;
                                //}
                            }
                        }
                        serializedMessage = null;
                        break;
                }
            }catch (SocketTimeoutException e){
                roundCounter++;
                round.incrementRound();
                phase = 1;
            }catch(IOException e){

            }
        }
        roundCounter++;
    }
}