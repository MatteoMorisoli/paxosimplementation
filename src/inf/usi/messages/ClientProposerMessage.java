package inf.usi.messages;

import java.io.Serializable;

public class ClientProposerMessage implements Serializable {
    private final int clientId;
    private final int valueToSend;

    public ClientProposerMessage(int clientId, int valueToSend){
        this.clientId = clientId;
        this.valueToSend = valueToSend;
    }

    public int getClientId() {
        return clientId;
    }

    public int getValueToSend() {
        return valueToSend;
    }

    @Override
    public String toString(){
        return "" + getClientId() + " " + getValueToSend();
    }
}
