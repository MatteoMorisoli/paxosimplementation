package inf.usi.messages;

import inf.usi.Round;

import java.io.Serializable;

public class AcceptorProposer2BMessage implements Serializable {
    private final int acceptorId;
    private final Round votedRound;
    private final int votedValue;

    public AcceptorProposer2BMessage(int acceptorId, Round votedRound, int votedValue) {
        this.acceptorId = acceptorId;
        this.votedRound = votedRound;
        this.votedValue = votedValue;
    }

    public int getAcceptorId() {
        return acceptorId;
    }

    public Round getVotedRound() {
        return votedRound;
    }

    public int getVotedValue() {
        return votedValue;
    }
}
