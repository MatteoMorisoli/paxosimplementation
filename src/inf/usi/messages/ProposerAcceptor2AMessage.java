package inf.usi.messages;

import inf.usi.Round;

import java.io.Serializable;

public class ProposerAcceptor2AMessage implements Serializable {

    private final Round currentRound;
    private final int currentValue;
    private final int proposerId;

    public ProposerAcceptor2AMessage(Round currentRound, int currentValue, int proposerId) {
        this.currentRound = currentRound;
        this.currentValue = currentValue;
        this.proposerId = proposerId;
    }

    public Round getCurrentRound() {
        return currentRound;
    }

    public int getCurrentValue() {
        return currentValue;
    }

    public int getProposerId() {
        return proposerId;
    }
}
