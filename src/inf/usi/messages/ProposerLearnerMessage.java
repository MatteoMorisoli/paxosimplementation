package inf.usi.messages;

import java.io.Serializable;

public class ProposerLearnerMessage implements Serializable {
    private final int clientId;
    private final int decidedValue;

    public ProposerLearnerMessage(int clientId, int decidedValue) {
        this.decidedValue = decidedValue;
        this.clientId = clientId;
    }

    public int getClientId() {
        return clientId;
    }

    public int getDecidedValue() {
        return decidedValue;
    }
}
