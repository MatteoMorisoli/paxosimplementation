package inf.usi.messages;

import inf.usi.Round;

import java.io.Serializable;

public class ProposerAcceptor1AMessage implements Serializable {
    private final int proposerId;
    private final int clientId;
    private final Round currentRound;


    public ProposerAcceptor1AMessage( int proposerId, int clientId, Round currentRound) {
        this.proposerId = proposerId;
        this.clientId = clientId;
        this.currentRound = currentRound;
    }

    public int getClientId() {
        return clientId;
    }

    public int getProposerId() {
        return proposerId;
    }

    public Round getCurrentRound() {
        return currentRound;
    }
}
