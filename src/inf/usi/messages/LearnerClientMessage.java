package inf.usi.messages;

import java.io.Serializable;

public class LearnerClientMessage implements Serializable{
    private final int clientId;
    private final int acceptedValue;

    public LearnerClientMessage(int clientId, int acceptedValue) {
        this.clientId = clientId;
        this.acceptedValue = acceptedValue;
    }

    public int getClientId() {
        return clientId;
    }

    public int getAcceptedValue() {
        return acceptedValue;
    }

}
