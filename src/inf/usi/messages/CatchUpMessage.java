package inf.usi.messages;

import java.io.Serializable;
import java.util.List;

public class CatchUpMessage implements Serializable{

    private final List<Integer> orderedValues;
    private final int learnerId;
    private final boolean moreMessages;

    public CatchUpMessage(List<Integer> orderedValues, int learnerId, boolean moreMessages) {
        this.orderedValues = orderedValues;
        this.learnerId = learnerId;
        this.moreMessages = moreMessages;
    }

    public List<Integer> getOrderedValues() {
        return orderedValues;
    }

    public int getLearnerId() {
        return learnerId;
    }

    public boolean isMoreMessages() {
        return moreMessages;
    }
}
