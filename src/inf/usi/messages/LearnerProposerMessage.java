package inf.usi.messages;

import java.io.Serializable;

public class LearnerProposerMessage implements Serializable {
    private final int learnerId;

    public LearnerProposerMessage(int learnerId) {
        this.learnerId = learnerId;
    }

    public int getLearnerId() {
        return learnerId;
    }
}
