package inf.usi.messages;

import inf.usi.Round;

import java.io.Serializable;

public class AcceptorProposer1BMessage implements Serializable{
    private final int acceptorId;
    private final Round round;
    private final Round votedRound;
    private final int votedValue;

    public AcceptorProposer1BMessage(int acceptorId, Round round, Round votedRound, int votedValue) {
        this.acceptorId = acceptorId;
        this.round = round;
        this.votedRound = votedRound;
        this.votedValue = votedValue;
    }

    public int getAcceptorId() {
        return acceptorId;
    }

    public Round getRound() {
        return round;
    }

    public Round getVotedRound() {
        return votedRound;
    }

    public int getVotedValue() {
        return votedValue;
    }
}
