package inf.usi;

import inf.usi.messages.ClientProposerMessage;
import inf.usi.messages.LearnerClientMessage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Client {

    private final int id;
    private final MultiCaster clientsChannel;
    private final MultiCaster proposersChannel;
    private final List<Integer> valuesToPropose;

    public Client(int id, NetworkInfo clientsInfo, NetworkInfo proposersInfo){
        this.id = id;
        this.proposersChannel = new MultiCaster(proposersInfo, false);
        this.clientsChannel = new MultiCaster(clientsInfo, true, 3500);
        this.valuesToPropose = retriveValuesToPropose();
    }

    public static void main(String[] args) {
        int id = Integer.parseInt(args[0]);
        ConfigReader configReader = new ConfigReader(args[1]);
        Map<String, NetworkInfo> infos = configReader.readConfig();
        Client client = new Client(id, infos.get("clients"), infos.get("proposers"));
        try {
            client.sendValues();
        } catch (InterruptedException e) {
            System.out.println("interruption exception in Client");
        }

    }

    private void sendValues() throws InterruptedException {
        ClientProposerMessage cpMessage;;
        boolean send = true;
        int i = 0;
        while(i < valuesToPropose.size()){
            if(send) {
                cpMessage = new ClientProposerMessage(id, valuesToPropose.get(i));
                proposersChannel.send(cpMessage);
            }
            try {
                Serializable serializableMessage = clientsChannel.receive();
                if(serializableMessage instanceof LearnerClientMessage){
                    LearnerClientMessage learnerMessage = (LearnerClientMessage) serializableMessage;
                    if(learnerMessage.getClientId() == id && learnerMessage.getAcceptedValue() == valuesToPropose.get(i)) {
                        i++;
                    }
                }
            } catch (SocketTimeoutException e) {

            }catch (IOException e){

            }
        }
    }

    private List<Integer> retriveValuesToPropose(){
        List<Integer> valuesToPropose = new ArrayList<Integer>();
        InputStreamReader isReader = new InputStreamReader(System.in);
        BufferedReader buffReader = new BufferedReader(isReader);
        String line;
        try {
            while ((line = buffReader.readLine()) != null) {
                Integer value = Integer.parseInt(line);
                valuesToPropose.add(value);
            }
        } catch (IOException e1) {
            System.out.println("error providing values at stdin");
            e1.printStackTrace();
        }
        return valuesToPropose;
    }

}
