package inf.usi;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class NetworkInfo {

    private final InetAddress ip;
    private final int port;

    public NetworkInfo(String ip, String port){InetAddress ip1;
        try {
            ip1 = InetAddress.getByName(ip);
        } catch (UnknownHostException e) {
            System.out.println("Unrecognised IP");
            ip1 = null;
            e.printStackTrace();
        }
        this.ip = ip1;
        this.port = Integer.parseInt(port);
    }

    public NetworkInfo(InetAddress ip, int port){
        this.ip = ip;
        this.port = port;
    }

    public InetAddress getIp() {
        return ip;
    }

    public int getPort() {
        return port;
    }
}
